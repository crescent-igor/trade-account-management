package com.mycompany.jenkinsapp.jenkins_hands_on;

import static org.junit.Assert.*;

import org.junit.Test;

public class TradeTest {

	@Test
	public void regularAccountTest() {
		Trade t1 = new Trade(new RegularAccountStrategy(), 1, "", "", 17, "");
		t1.calculateFee();
		assertEquals(t1.getFee(), 1.7, 0.0001);

	}

	@Test
	public void eliteAccountTest() {
		Trade t2 = new Trade(new EliteAccountStrategy(), 1, "", "", 15, "");
		t2.calculateFee();
		assertEquals(t2.getFee(), 1, 0);

	}

	@Test
	public void primeAccountTest() {
		Trade t3 = new Trade(new PrimeAccountStrategy(), 1, "", "", 10, "");
		t3.calculateFee();
		assertEquals(t3.getFee(), 1.2, 0);
	}

}
