package com.mycompany.jenkinsapp.jenkins_hands_on;

public class RegularAccountStrategy implements AccountStrategy{
	public String getAccountType() {
		return "Regular";
	};
	public double calculateTradeFee(long Qty) {
		return 0.1*Qty;
	};

}
