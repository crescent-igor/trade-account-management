package com.mycompany.jenkinsapp.jenkins_hands_on;

public class Trade {

	private AccountStrategy accountStrategy;
	private int id;
	private String contract;
	private String verb;
	private long qty;
	private String client;
	private double fee;

	Trade(AccountStrategy accountStrategy, int id, String contract, String verb, long qty, String client) {
		this.accountStrategy = accountStrategy;
		this.id = id;
		this.contract = contract;
		this.verb = verb;
		this.setQty(qty);
		this.client = client;
	}

	public void calculateFee() {
		fee = accountStrategy.calculateTradeFee(qty);
	}

	public String getAccountType() {
		return accountStrategy.getAccountType();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContract() {
		return contract;
	}

	public void setContract(String contract) {
		this.contract = contract;
	}

	public String getVerb() {
		return verb;
	}

	public void setVerb(String verb) {
		this.verb = verb;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public long getQty() {
		return qty;
	}

	public void setQty(long qty) {
		this.qty = qty;
	}

	public double getFee() {
		return fee;
	}

}
