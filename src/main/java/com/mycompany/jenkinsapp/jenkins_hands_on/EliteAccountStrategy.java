package com.mycompany.jenkinsapp.jenkins_hands_on;

public class EliteAccountStrategy implements AccountStrategy {
	public String getAccountType() {
		return "Elite";
	};

	public double calculateTradeFee(long Qty) {
		return 1;
	};

}
