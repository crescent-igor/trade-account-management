package com.mycompany.jenkinsapp.jenkins_hands_on;
public class PrimeAccountStrategy implements AccountStrategy {
	public String getAccountType() {
		return "Prime";
	};

	public double calculateTradeFee(long Qty) {
		return 1.2;
	};

}
