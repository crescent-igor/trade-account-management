package com.mycompany.jenkinsapp.jenkins_hands_on;

public interface AccountStrategy {
	public String getAccountType();
	public double calculateTradeFee(long Qty);

}
